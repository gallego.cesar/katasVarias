package org.cesar;

import java.util.regex.Pattern;

/**
 * These kata it's about to find the coincident largest part
 * of two strings.
 * */
public class CoincidentPart {

	public static void main(String[] args) {
		String part = coincidentLoops("bgalway", "algalalwabsdf");
		System.out.println("Largest coincident part: " + part);
	}

	public static String coincidentLoops(String one, String two){
		// allways iterate the sortest string
		if(one.length()>two.length()){
			return coincidentLoops(two, one);
		}
		
		boolean find_coincident_part = true;
		int letters = 1;
		int i,idx;
		String curr_one,last_coincident = null;
		while(find_coincident_part){
			i=0;
			find_coincident_part = false;
			while(i<one.length()-letters && !find_coincident_part){
				
				if( i+letters>one.length() )
					idx = one.length()-1;
				else
					idx = i+letters;
				curr_one = one.substring(i, idx);
				
				if(two.contains(curr_one)){
					last_coincident=curr_one;
					find_coincident_part = true;
				}
				
				i++;
			}
			letters++;
		}
		return last_coincident;
	}
}
