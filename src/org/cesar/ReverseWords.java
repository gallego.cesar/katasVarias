package org.cesar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * These Kata it's about reverse words.
 * Words are supplied in a char array.
 * */
public class ReverseWords {

	public static void main(String[] args) {
		char[] letters = new char[]{'h','i',' ','w','o','r','l','d'};
		List<Character> inverse = new ArrayList<>();

		int current_insertion_point=0;
		for(int i=letters.length-1; i>=0; i--){
			if(letters[i] == ' '){
				current_insertion_point=inverse.size();
			}
			inverse.add(current_insertion_point, letters[i]);
			if(letters[i] == ' '){
				current_insertion_point=inverse.size();
			}
		}
		
		System.out.println(Arrays.toString(inverse.toArray()));
	}

}
